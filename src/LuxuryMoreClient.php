<?php

namespace SDK;


use GuzzleHttp\Client;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class LuxuryMoreClient
{
    private static $appId;
    private static $appSecret;

    private static $API_BASE_URL = 'http://127.0.0.1:8000';
    private static $CLAIM_USER_ID = 'userId';
    private static $CLAIM_PHONE_NUMBER = 'phoneNumber';
    private static $CLAIM_WE_CHAT_UNION_ID = 'weChatUnionId';
    private static $CLAIM_APP_ID = "appId";

    public static function initialize($appId, $appSecret)
    {
        static::$appId = $appId;
        static::$appSecret = $appSecret;
    }

    /**
     * 通过手机号码获取访问令牌
     * @param string $phoneNumber 手机号码
     * @return array
     * 成功：{"token":"token"}
     * 失败：{"code":错误代码,"message":"失败原因"}
     */
    public static function getTokenByPhoneNumber($phoneNumber)
    {
        $signature = static::buildSignature();
        $client = new Client();
        $response = $client->post(static::$API_BASE_URL . '/users/phoneNumberIs/' . $phoneNumber . '/authenticatedWithSignature/' . $signature . '/tokens');
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * 通过微信联合ID获取访问令牌
     * @param string $weChatUnionId 微信联合ID
     * @return array
     * 成功：{"token":"访问令牌"}
     * 失败：{"code":错误代码,"message":"失败原因"}
     */
    public static function getTokenByWeChatUnionId($weChatUnionId)
    {
        $signature = static::buildSignature();
        $client = new Client();
        $response = $client->post(static::$API_BASE_URL . '/users/weChatUnionIdIs/' . $weChatUnionId . '/authenticatedWithSignature/' . $signature . '/tokens');
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * 用户注册
     * @param string $phoneNumber 手机号码
     * @param string $weChatUnionId 微信联合ID
     * @return array
     * 成功：{"user_id":"用户ID","phone_number":"手机号码","we_chat_union_id":"微信联合ID"}
     * 失败：{"code":错误代码,"message":"失败原因"}
     */
    public static function registerUser($phoneNumber, $weChatUnionId)
    {
        $signature = static::buildSignature();
        $client = new Client();
        $response = $client->post(static::$API_BASE_URL . '/apps/authenticatedWithSignature/' . $signature . '/users', array('json' => array('phone_number' => $phoneNumber, 'we_chat_union_id' => $weChatUnionId)));
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * 通过短信获取验证码
     * @param string $phoneNumber 手机号码
     * @param $checkCodeUsage 1:验证码登录 4:绑定手机号码
     * @return array
     * 成功：{"check_code":"验证码"}
     * 失败：{"code":错误代码,"message":"失败原因"}
     */
    public static function getCheckCodeByShortMessage($phoneNumber, $checkCodeUsage)
    {
        $signature = static::buildSignature();
        $client = new Client();
        $response = $client->post(static::$API_BASE_URL . '/apps/authenticatedWithSignature/' . $signature . '/phoneNumber/' . $phoneNumber . '/withUsage/' . $checkCodeUsage . '/checkCodes');
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * 通过短信获取验证码
     * @param string $phoneNumber 手机号码
     * @param string $checkCodeUsage 1:验证码登录 4:绑定手机号码
     * @param string $checkCode 验证码
     * @return array
     * 成功：{"verify":true}
     * 失败：{"code":错误代码,"message":"失败原因"}
     */
    public static function verifyShortMessageCheckCode($phoneNumber, $checkCodeUsage, $checkCode)
    {
        $signature = static::buildSignature();
        $client = new Client();
        $response = $client->get(static::$API_BASE_URL . '/apps/authenticatedWithSignature/' . $signature . '/phoneNumber/' . $phoneNumber . '/withUsage/' . $checkCodeUsage . '/isVerifyCheckCode/' . $checkCode);
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * 获取访问令牌附带的数据
     * @param string $token
     * @return null|TokenPayload
     */
    public static function getTokenPayload($token)
    {
        $token = (new Parser())->parse($token);
        $userId = $token->getClaim(static::$CLAIM_USER_ID);
        $phoneNumber = $token->getClaim(static::$CLAIM_PHONE_NUMBER);
        $weChatUnionId = $token->getClaim(static::$CLAIM_WE_CHAT_UNION_ID);

        if (!$token->verify(new Sha256(), static::$appSecret)) {
            return null;
        }

        return new TokenPayload($userId, $phoneNumber, $weChatUnionId);
    }

    private static function buildSignature()
    {
        $signature = (new Builder())
            ->setExpiration(time() + 3600)
            ->set(static::$CLAIM_APP_ID, static::$appId)
            ->sign(new Sha256(), static::$appSecret)
            ->getToken();
        return (string)$signature;
    }

}