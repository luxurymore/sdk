<?php

namespace SDK\Test;

use PHPUnit_Framework_TestCase;
use SDK\LuxuryMoreClient;

class LuxuryMoreClientTests extends PHPUnit_Framework_TestCase
{
    private $FIXTURE_PHONE_NUMBER = '13612345678';
    private $FIXTURE_WE_CHAT_UNION_ID = 'g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4';

    public function setUp()
    {
        LuxuryMoreClient::initialize('36E3BB6C-B06D-4A3A-A0BD-7394D27A7774', 'NzE3OWI4ZmZlZDQ1YzdlMzg3ZWYwZDIxZmEwZjRlOTA=');
        LuxuryMoreClient::initialize('3DF69C35-2D2D-4C6C-A70F-C2431D27C661', 'YjJmNzk5OTA1ZmJhNzIyYzQxZWNiZWQ4ZGNlZTE2ZDg=');
        $this->FIXTURE_PHONE_NUMBER = '15180105621';
        $this->FIXTURE_WE_CHAT_UNION_ID = 'oOzsSwuZT99dk25g3aWZ1JYNAWdA';
    }

//    public function testRegisterUserSuccess()
//    {
//        $randomPhoneNumber = '136' . rand(10000000, 99999999);
//        $randomWeChatUnionId = 'WX' . rand(10000000, 99999999);
//        $result = LuxuryMoreClient::registerUser($randomPhoneNumber, $randomWeChatUnionId);
//        self::assertEquals($randomPhoneNumber, $result['phone_number']);
//        self::assertEquals($randomWeChatUnionId, $result['we_chat_union_id']);
//    }

//    public function testGetTokenByPhoneNumberSuccess()
//    {
//        $result = LuxuryMoreClient::getTokenByPhoneNumber($this->FIXTURE_PHONE_NUMBER);
//        self::assertNotEmpty($result['token']);
//        $tokenPayload = LuxuryMoreClient::getTokenPayload($result['token']);
//        self::assertEquals($this->FIXTURE_PHONE_NUMBER, $tokenPayload->phoneNumber());
//        self::assertEquals($this->FIXTURE_WE_CHAT_UNION_ID, $tokenPayload->weChatUnionId());
//        self::assertNotEmpty($tokenPayload->userId());
//    }

    public function testGetTokenByWeChatUnionIdSuccess()
    {
        $result = LuxuryMoreClient::getTokenByWeChatUnionId($this->FIXTURE_WE_CHAT_UNION_ID);
        self::assertNotEmpty($result['token']);
        $tokenPayload = LuxuryMoreClient::getTokenPayload($result['token']);
        self::assertEquals($this->FIXTURE_PHONE_NUMBER, $tokenPayload->phoneNumber());
        self::assertEquals($this->FIXTURE_WE_CHAT_UNION_ID, $tokenPayload->weChatUnionId());
        self::assertNotEmpty($tokenPayload->userId());
    }

//    public function testGetCheckCodeByShortMessageSuccess()
//    {
//        $result = LuxuryMoreClient::getCheckCodeByShortMessage($this->FIXTURE_PHONE_NUMBER, 1);
//        self::assertNotEmpty($result['check_code']);
//    }
//
//    public function testVerifyCheckCodeSuccess()
//    {
//        $result = LuxuryMoreClient::getCheckCodeByShortMessage($this->FIXTURE_PHONE_NUMBER, 1);
//        $result = LuxuryMoreClient::verifyShortMessageCheckCode($this->FIXTURE_PHONE_NUMBER, 1, $result['check_code']);
//        self::assertTrue($result['is_verify']);
//    }
}